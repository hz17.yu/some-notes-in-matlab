# Some notes in Matlab

Keep records of learning matlab, especially in numerical applications.

**Before all, in matlab, type 'doc anything' in the Command Window in matlab to go to documentation for anything.**

1. efficient way to do operations with large scales: e.g., substitute large scale of data into a symbolic matrix or array: 

turn data into cell (mat2cell), turn symbolic expression into matlabFunction, and use cellfun() to the function with the data cell as input. Much faster than subs(). 
